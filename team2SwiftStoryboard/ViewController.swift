//
//  ViewController.swift
//  team2SwiftStoryboard
//
//  Created by Mohammad Jebelli on ۲۰۱۷/۱/۳۱.
//  Copyright © ۲۰۱۷ cs3260. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label1: UIStepper!
    @IBOutlet weak var label2: UIStepper!
    
   
    @IBOutlet weak var team1Lbl: UILabel!

    @IBAction func stepperChanged(_ sender: UIStepper) {
        team1Lbl.text = String(sender.value.description)
    }
   
    
    
    @IBOutlet weak var team2Lbl: UILabel!
    @IBAction func stepperChanged2(_ sender: UIStepper) {
    team2Lbl.text = String(sender.value.description)
    }
    
    
    @IBAction func resetBtnTouched(_ sender: UIButton) {
        //Set steppers to 0
        label1.value=0
        team1Lbl.text = String(0)
        
        //Set the labels to 0
        label2.value=0
        team2Lbl.text = String(0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

